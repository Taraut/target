var app = require('express')();

var http = require('http').createServer(app);

var io = require('socket.io')(http, {
    cors: {
        origin: 'http://127.0.0.1:3000',
    },
});

const PORT = 8080;

/*
    DEFAULTS
*/
/* simulate M16A2 with NATO STANAG 4179 30-round magazine burst fire on a M16A2 25 Meter Zeroing Target */

// target grid size
var gridX = 54;
var gridY = 20;
// random bullet deviation in auto (burst) fire mode
var autoDx = 12;
var autoDy = 6;
// auto fire (shots)
var autoFire = 3;
// mag capacity
var mag = 30;
// min aiming time (sec)
var aimingTime = 1;

/* ---------------------------------------------- */

var getRandomInt = (maxInt) => {
    return Math.floor(Math.random() * Math.floor(maxInt));
};

http.listen(PORT, () => {
    console.log(`Shooting listening on port: ${PORT}`);
});

aimingTime = aimingTime * 1000;

var getShooting = (socket) => {
    var shots = [];
    // first shot
    shots[0] = { x: getRandomInt(gridX), y: getRandomInt(gridY) };
    socket.emit('connection', shots[0]);

    // random bullet deviation in auto (burst) fire mode calculation
    for (var indexAutoFire = 1; indexAutoFire < autoFire; indexAutoFire++) {
        shots[indexAutoFire] = {
            x: shots[indexAutoFire - 1].x - autoDx / 2 + getRandomInt(autoDx),
            y: shots[indexAutoFire - 1].y - autoDy / 2 + getRandomInt(autoDy),
        };
    }

    // bullet hit target (detected) in auto (burst) mode
    for (var indexAutoFire = 1; indexAutoFire < autoFire; indexAutoFire++) {
        if (
            shots[indexAutoFire].x < gridX &&
            shots[indexAutoFire].x > 0 &&
            shots[indexAutoFire].y < gridY &&
            shots[indexAutoFire].y > 0
        ) {
            socket.emit('connection', shots[indexAutoFire]);
        }
    }
};

io.on('connection', (socket) => {
    socket.on('em_stop', () => {
        console.log('"Emergency stop" is activated from client side');
        socket.disconnect();
    });

    var aimingDelay = 0;

    for (var index = 0; index < mag; index = index + autoFire) {
        // aiming delay
        aimingDelay = aimingDelay + aimingTime + getRandomInt(1000);

        setTimeout(getShooting, aimingDelay, socket);
    }
});
