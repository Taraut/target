Allen Joelson
Riga, 1 January 2021

                    SIMPLE VIEW OF THE PRACTICE TARGET
                    ----------------------------------

                    CONTENTS

PREFACE

SECURITY WARNING

CHAPTER 1. INTRODUCTION
Section I. Software Overview
            1-1. Structure
Section II. Hits Representation
            1-2. Burst Separation
            1-3. General Hits Representation
            1-4. Statistical Data
Section III. Operations
            1-5. Data Collection
            1-6. Emergency Stop

CHAPTER 2. TECHNICAL IMPLEMENTATION
Section I. Web Server
            2-1. General Information
            2-2. Code Location
            2-3. Default Url
            2-4. Server Start
            2-5. Defaults Change
Section II. Client Part
            2-6. General Information
            2-7. Code Location
            2-8. Default Url
            2-9. Server Start
            2-10. Run Mode
            2-11. Safety Critical Elements
            2-12. Multi Language Support
            2-13. Jest Unit Tests Start

Section III. Availability


                    PREFACE

This manual provides guidance for SIMPLE VIEW OF THE PRACTICE TARGET (SVOPT) software.
You may also contact for more information:
E:mail allen.joelson@gmail.com
Phone (371) 295-37455


                    SECURITY WARNING

Some UI elements use China made JavaScript library (https://ant.design/).
Also, no any security check for other parts of this software is done.
Check your policies before copy or run SVOPT software on devices with sensitive information.


                    CHAPTER 1.
                    INTRODUCTION

                    Section I. SOFTWARE OVERVIEW

As a simplified representation SVOPT simulate M16A2 with NATO STANAG 4179 30-round magazine burst fire on a M16A2 25 Meter Zeroing Target.
Aiming interval between bursts is random set to 1-2sec.
SVOPT provide graphical representation and statistical data of hits and firing mode.

1-1.	STRUCTURE

SVOPT consist of 2 separate independent parts:
1.	A Web Server (WS) that at random time generates random events that represent bullet hits into the target.
2.	A Client Part (CP) that provide graphical representation and statistical data of hits and firing mode with easy extendable multi language support.

                    Section II. HITS REPRESENTATION

1-2. BURST SEPARATION

Burst separation is based on “aiming” time delay between trigger pressings.
By default it is 0.5 seconds.

1-3. GENERAL HITS REPRESENTATION

Hits are represented on a target in a blue color.
Current (last) burst is represented in a red color.
Cursor pointing (or click) on hit or near area will show hint with:
X-position
Y-position
id – burst number
iid – hit number in burst
Burst hits will have yellow color.

1-4. STATISTICAL DATA

Statistical data is present in a separate table.
Hits are separated by burst.
Table legend has hints with description. For to see hint, point cursor or click on description in question.
Data in table:
ID – burst number
Hts – hits in burst
Xmd0 – accuracy by X axis. (Hits median deviation by X axis from target center.)
Ymd0 – accuracy by Y axis. (Hits median deviation by Y axis from target center.)
Xmd – consistency by X axis. (Median hits deviation inside burst by X axis.)
Ymd – consistency by Y axis. (Median hits deviation inside burst by Y axis.)
Click on table for to see burst on a target.
Burst will be highlighted in green color.

                    Section III. OPERATIONS

1-5. DATA COLLECTION

Pressing ‘FIRING LINE CLEAR’ reset all collected data and starts new data collection.

1-6. EMERGENCY STOP

"Emergency stop" button sends command to server to stop data generation and sending.
As a safety measure after Emergency stop activation ‘FIRING LINE CLEAR’ button is locked, preventing accidental start.
Restart CP to continue data collection.


                    CHAPTER 2.
                    TECHNICAL IMPLEMENTATION

                    Section I. WEB SERVER

2-1. GENERAL INFORMATION

WS part runs on Node.js

2-2. CODE LOCATION

WS code located in folder: /shooting

2-3. DEFAULT URL

url: 'http://127.0.0.1:8080'

2-4. SERVER START

WS utilize environment variables (.env) and must starting with preloading.
Starting command: node -r dotenv/config server.js

2-5. DEFAULTS CHANGE

Default parameters change could be easy done in server.js file in special section.

                    Section II. CLIENT PART

2-6. GENERAL INFORMATION

CP frontend based on React JavaScript library.

2-7. CODE LOCATION

CP code located in folder: /public

2-8. DEFAULT URL

url: 'http://127.0.0.1:3000'
Use of url: 'http://localhost:3000' is not recommended due to CORS policies.

2-9. SERVER START

WS utilize environment variables (.env) and must be starting with preloading.
Starting command: node -r dotenv/config server.js
Starting w/o backend microservice support (not recommended): http-server -c-1

2-10. RUN MODE

By default webpack and index.html configurations are set to development mode.

2-11. SAFETY CRITICAL ELEMENTS

Safety critical elements use simplified code (standard React elements w/o graphic libraries) and inline CSS code.

2-12. MULTI LANGUAGE SUPPORT

Code structure allow easy add languages and change texts.
All same language texts are in one separate file.

2-13. JEST UNIT TESTS START
For to start Jest unit tests execute: npm test

                    Section III. AVAILABILITY

SVOPT software is available from GitLab repository: https://gitlab.com/Taraut/target
