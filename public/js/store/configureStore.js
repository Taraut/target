import { createBrowserHistory } from 'history';
import { applyMiddleware, compose, createStore } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import { createRootReducer } from '../redux/reducers';

export const history = createBrowserHistory();

export default function configureStore(preloadedState) {
    // console.log('REDUX_DEVTOOLS_EXTENSION is not commented in configureStore.js');
    const store = createStore(
        createRootReducer(history), // root reducer with router state
        preloadedState,
        compose(
            applyMiddleware(
                routerMiddleware(history) // for dispatching history actions
                // ... other middlewares ...
            ),
            // test use only comment on prod
            // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
        )
    );

    return store;
}
