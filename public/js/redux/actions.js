const getLanguage = ({ language = 'eng' } = {}) => ({
    type: 'GET_LANGUAGE',
    language: language,
});

const getTexts = ({ texts = {} } = {}) => ({
    type: 'GET_TEXTS',
    texts: texts,
});

const getWindowSize = ({ windowSize = {} } = {}) => ({
    type: 'GET_WINDOW_SIZE',
    windowSize: windowSize,
});

const getProdList = ({ prodList = {} = {}}) => ({
    type: 'GET_PROD_LIST',
    prodList: prodList,
})

export { getLanguage, getTexts, getWindowSize, getProdList };
