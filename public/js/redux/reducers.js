import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

const initialState = [
    {
        language: 'eng',
        texts: {},
        windowSize: {
            height: 777,
            width: 999,
        },
        prodList: [],
    },
];

const itemsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_LANGUAGE':
            return Object.assign({}, state, {
                ...state,
                language: action.language,
            });

        case 'GET_TEXTS':
            return Object.assign({}, state, {
                ...state,
                texts: action.texts,
            });
        case 'GET_WINDOW_SIZE':
            return Object.assign({}, state, {
                ...state,
                windowSize: action.windowSize,
            });
        case 'GET_PROD_LIST':
            return Object.assign({}, state, {
                ...state,
                prodList: action.prodList,
            });

        default:
            return state;
    }
};

const createRootReducer = (history) =>
    combineReducers({
        router: connectRouter(history),
        itemsReducer: itemsReducer,
    });

export { createRootReducer };
