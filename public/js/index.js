import React, { Suspense, lazy } from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { Route, Switch } from 'react-router-dom';
import { Row, Col } from 'antd';
import { SpinC } from './components/atoms/SpinC';

import configureStore, { history } from './store/configureStore';

const store = configureStore();

const Header = lazy(() => import('./components/Header'));
const HomePage = lazy(() => import('./components/HomePage'));

const jsx = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Suspense fallback={<SpinC />}>
                <Row>
                    <Col span={2} />
                    <Col span={20}>
                        <Header />
                    </Col>
                    <Col span={2} />
                </Row>

                <Row>
                    <Col span={2} />
                    <Col span={20} className="mainPart">
                        <nav>
                            <Switch>
                                <Route exact path="/" component={HomePage} />
                            </Switch>
                        </nav>
                    </Col>
                    <Col span={2} />
                </Row>
            </Suspense>
        </ConnectedRouter>
    </Provider>
);

render(jsx, document.getElementById('root'));
