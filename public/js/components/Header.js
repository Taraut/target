import React from 'react';
import LanguageSelector from './molecules/languageSelector';
import { connect } from 'react-redux';
import { getWindowSize } from '../redux/actions';
import { mediaData } from '../defaults';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ws: {
                width: 555,
                height: 666,
            },
        };

        this.windowSizeProcess = this.windowSizeProcess.bind(this);
    }

    componentDidMount() {
        this.windowSizeProcess();
        window.addEventListener('resize', this.windowSizeProcess);
    }

    componentWillUnmount() {
        window.addEventListener('resize', this.windowSizeProcess);
    }

    windowSizeProcess() {
        const ws = {
            width: window.innerWidth,
            height: window.innerHeight,
        };
        this.setState({
            ws: ws,
        });
        this.props.dispatch(getWindowSize({ windowSize: ws }));
    }

    render() {
        const { ws } = this.state;

        // Desktop > 1024px
        if (ws.width > mediaData.tabletMaxWidth) {
            return (
                <div className="header_wrapper">
                    <div className="header_firstLine">
                        <div></div>

                        <div className="header_languageSelector">
                            <LanguageSelector />
                        </div>
                    </div>
                    <div></div>
                </div>
            );
        }

        // Tablet and Mobile <1024px
        return (
            <div className="header_wrapper">
                <div className="header_firstLine">
                    <div></div>
                    <div className="header_languageSelector">
                        <LanguageSelector />
                    </div>
                </div>
                <div className="header_secondLine"></div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        windowSize: state.itemsReducer.windowSize,
    };
};

export default connect(mapStateToProps)(Header);
export { Header };
