import React from 'react';
import PropTypes from 'prop-types';

const TextSubtitle = ({ text }) => {
    return <div className="text_subtitle">{text}</div>;
};

TextSubtitle.propTypes = {
    text: PropTypes.string,
};
TextSubtitle.defaultProps = {
    text: '',
};

export { TextSubtitle };
