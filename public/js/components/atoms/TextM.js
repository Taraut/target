import React from 'react';
import PropTypes from 'prop-types';

const TextM = ({ text }) => {
    return <div className="text_m">{text}</div>;
};

TextM.propTypes = {
    text: PropTypes.string,
};
TextM.defaultProps = {
    text: '',
};

export { TextM };
