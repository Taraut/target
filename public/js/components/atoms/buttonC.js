import React from 'react';
import PropTypes from 'prop-types';
import { Button, Icon } from 'antd';

const ButtonC = ({ data = {}, text, onClick, disabled }) => {
    const {
        type = 'primary',
        size = 'large',
        iconType = false,
        htmlType = 'button',
        danger = false,
    } = data;

    return (
        <Button type={type} size={size} onClick={onClick} disabled={disabled} danger={danger} htmlType={htmlType} className="buttonC">
            {iconType && <Icon type={iconType} />}

            <span className="ButtonC_text">{text}</span>
        </Button>
    );
};

ButtonC.propTypes = {
    data: PropTypes.object,
    onClick: PropTypes.func,
    text: PropTypes.string.isRequired,
};

ButtonC.defaultProps = {
    onClick: () => void 0,
    disabled: false,
    text: '',
};

export { ButtonC };
