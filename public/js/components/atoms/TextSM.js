import React from 'react';
import PropTypes from 'prop-types';

const TextSM = ({ text }) => {
    return <div className="text_sm">{text}</div>;
};

TextSM.propTypes = {
    text: PropTypes.string,
};
TextSM.defaultProps = {
    text: '',
};

export { TextSM };
