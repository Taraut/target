import React from 'react';
import socketIOClient from 'socket.io-client';
import { connect } from 'react-redux';
import {
    XYPlot,
    MarkSeries,
    XAxis,
    YAxis,
    VerticalGridLines,
    HorizontalGridLines,
    Hint,
} from 'react-vis';
import { Tooltip, notification } from 'antd';
import { TextSM } from './atoms/TextSM';
import { ButtonC } from './atoms/buttonC';
import { dataProcessing } from './molecules/dataProcessing/dataProcessing';

class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            shootingData: [],
            currentBurst: [],
            hintData: false,
            hintBurst: false,
            burstData: [],
            burstTableIdHits: [],
            connect: false,
            socket: false,
            fireDisable: false,
            url: 'http://127.0.0.1:8080',
        };
    }

    fire = () => {
        const { url, shootingData, burstData } = this.state;

        const socket = socketIOClient(url);

        this.setState({ socket: socket });

        socket.on(
            'connection',
            (data) => {
                const dataForProc = {
                    x: data.x - 27,
                    y: data.y - 10,
                    timestamp: Math.floor(Date.now()),
                };

                const shootingDataPr = dataProcessing(shootingData, dataForProc, burstData);

                this.setState({
                    connected: true,
                    shootingData: shootingDataPr.shootingData,
                    currentBurst: shootingDataPr.currentBurst,
                    burstData: shootingDataPr.burstData,
                });
            },
            []
        );
    };

    onClickFire = () => {
        const { socket } = this.state;

        if (socket) {
            socket.disconnect();
        }

        this.setState(
            {
                shootingData: [],
                currentBurst: [],
                hintData: false,
                hintBurst: false,
                burstData: [],
                burstTableIdHits: [],
            },
            () => {
                this.fire();
            }
        );
    };

    hintDataSpp = (NearestXY) => {
        const hintBurst = this.state.shootingData.filter((element) => element.id == NearestXY.id);

        this.setState({
            hintBurst: hintBurst,
            hintData: {
                x: NearestXY.x,
                y: NearestXY.y,
                id: NearestXY.id,
                iid: NearestXY.iid,
            },
        });
    };

    burstTableOnClick = (id) => {
        const burstTableIdHits = this.state.shootingData.filter((element) => element.id == id);
        this.setState({
            burstTableIdHits: burstTableIdHits,
        });
    };

    emStopOnClick = () => {
        const { socket } = this.state;

        if (socket) {
            socket.emit('em_stop', {});
        }

        this.setState({
            fireDisable: true,
        });

        let emStopAlertMsg = '';
        let emStopAlertdSC = '';

        if (this.props.texts && this.props.texts.home) {
            ({ emStopAlertMsg, emStopAlertdSC } = this.props.texts.home);
        }

        notification.open({
            message: emStopAlertMsg,
            description: emStopAlertdSC,
            duration: 0,
        });
    };

    render() {
        const {
            shootingData,
            currentBurst,
            hintData,
            hintBurst,
            burstData,
            burstTableIdHits,
            fireDisable = true,
        } = this.state;

        let title = '';
        let tableId = '';
        let tableHts = '';
        let tableXmd0 = '';
        let tableYmd0 = '';
        let tableXmd = '';
        let tableYmd = '';
        let fire = '';
        let emStop = 'Emergency stop';

        if (this.props.texts && this.props.texts.home) {
            ({
                title,
                tableId,
                tableHts,
                tableXmd0,
                tableYmd0,
                tableXmd,
                tableYmd,
                fire,
                emStop,
            } = this.props.texts.home);
        }

        if (!emStop) {
            emStop = 'Emergency stop';
        }

        const xTickGrid = [
            -3,
            -6,
            -9,
            -12,
            -15,
            -18,
            -21,
            -24,
            -27,
            0,
            3,
            6,
            9,
            12,
            15,
            18,
            21,
            24,
            27,
        ];

        const yTickGrid = [
            -1,
            -2,
            -3,
            -4,
            -5,
            -6,
            -7,
            -8,
            -9,
            -10,
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
        ];

        return (
            <div>
                {title && <TextSM text={title} />}

                <div className="home_block_wrapper">
                    <div className="home_block">
                        <XYPlot
                            xDomain={[-27, 27]}
                            yDomain={[-10, 10]}
                            width={500}
                            height={500}
                            className="target_m16a2_25m"
                            onMouseLeave={() => this.setState({ hintData: false, hintData: false })}
                        >
                            <XAxis tickValues={xTickGrid} />
                            <YAxis tickValues={yTickGrid} />
                            <VerticalGridLines tickValues={xTickGrid} />
                            <HorizontalGridLines tickValues={yTickGrid} />
                            <MarkSeries
                                data={shootingData}
                                onNearestXY={(NearestXY) => this.hintDataSpp(NearestXY)}
                            />

                            {this.state.hintData ? <Hint value={hintData} /> : null}
                            <MarkSeries data={currentBurst} color="#cc0000" />
                            {this.state.hintData ? (
                                <MarkSeries data={hintBurst} color="#cca300" />
                            ) : null}
                            {burstTableIdHits ? (
                                <MarkSeries data={burstTableIdHits} color="#33cc33" />
                            ) : null}
                        </XYPlot>
                    </div>

                    <div className="home_block">
                        <table className="burstTable">
                            <thead>
                                <tr>
                                    <th>
                                        <Tooltip placement="topLeft" title={tableId}>
                                            <span>ID</span>
                                        </Tooltip>
                                    </th>
                                    <th>
                                        <Tooltip placement="topRight" title={tableHts}>
                                            <span>Hts</span>
                                        </Tooltip>
                                    </th>
                                    <th>
                                        <Tooltip placement="topRight" title={tableXmd0}>
                                            <span>Xmd0</span>
                                        </Tooltip>
                                    </th>
                                    <th>
                                        <Tooltip placement="topRight" title={tableYmd0}>
                                            <span>Ymd0</span>
                                        </Tooltip>
                                    </th>
                                    <th>
                                        <Tooltip placement="topRight" title={tableXmd}>
                                            <span>Xmd</span>
                                        </Tooltip>
                                    </th>
                                    <th>
                                        <Tooltip placement="topRight" title={tableYmd}>
                                            <span>Ymd</span>
                                        </Tooltip>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {burstData.map((element) => (
                                    <tr
                                        key={element.id}
                                        onClick={() => this.burstTableOnClick(element.id)}
                                    >
                                        <td>{element.id}</td>
                                        <td>{element.hts}</td>
                                        <td>{element.xmd0}</td>
                                        <td>{element.ymd0}</td>
                                        <td>{element.xmd}</td>
                                        <td>{element.ymd}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="home_buttons_block">
                    <button
                        onClick={this.emStopOnClick}
                        style={{
                            backgroundColor: '#ff0000',
                            color: ' #ffffff ',
                            fontWeight: 'bold',
                        }}
                    >
                        {emStop}
                    </button>
                    <ButtonC text={fire} onClick={this.onClickFire} disabled={fireDisable} />
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        texts: state.itemsReducer.texts,
    };
};

export default connect(mapStateToProps)(HomePage);
export { HomePage };
