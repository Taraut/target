import { median } from 'mathjs';

const mdCalc = (mdArr) => {
    let dData = [];
    // calc dist between hits
    for (var i = 0; i <= mdArr.length - 2; i++) {
        for (var ii = i + 1; ii <= mdArr.length - 1; ii++) {
            dData.push(Math.abs(mdArr[i] - mdArr[ii]));
        }
    }

    return median(dData);
};

export { mdCalc };
