// No target, or fire mode specific settings here
/*
xmd0: x - axis median to zero,
ymd0: y - axis median to zero,
xmd: x - axis median between burst hits,
ymd: y - axis median between burst hits
*/

import { burstDataCalc } from "./burstDataCalc";

const dataProcessing = (
    shootingData,
    dataForProc = { x: 0, y: 0, timestamp: 0 },
    burstData = [],
) => {
    const { x, y, timestamp } = dataForProc;

    let id, iid;
    let currentBurst = [];

    // first shot
    if (!shootingData || shootingData.length == 0) {
        id = 1;
        iid = 1;
    }

    if (shootingData && shootingData.length > 0) {
        const sDlastElement = shootingData[shootingData.length - 1];

        id = sDlastElement.id;
        iid = sDlastElement.iid + 1;

        if (timestamp - sDlastElement.timestamp >= 500) {
            id = sDlastElement.id + 1;
        }

        if (sDlastElement.id != id) {
            iid = 1;
        }
        if (sDlastElement.id == id) {
            iid = sDlastElement.iid + 1;

            currentBurst = shootingData.filter((element) => element.id == id);
        }
    }

    currentBurst.push({
        x: x,
        y: y,
        timestamp: timestamp,
        id: id,
        iid: iid,
    });

    shootingData.push({
        x: x,
        y: y,
        timestamp: timestamp,
        id: id,
        iid: iid,
    });

    const burstDataC = burstDataCalc(burstData, id, iid, shootingData);

    const bigData = {
        shootingData: shootingData,
        currentBurst: currentBurst,
        burstData: burstDataC,
    };

    return bigData;
};

export { dataProcessing };
