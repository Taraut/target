import { mdCalc } from './mdCalc';

const mdArr = [2, -2, -5];
const dData = 4;

test('mdCalc', () => {
    expect(mdCalc(mdArr)).toBe(dData);
});
