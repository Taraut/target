import { median } from 'mathjs';
import {mdCalc} from './mdCalc';

const burstDataCalc = (burstData, id, iid, shootingData) => {

    if (iid == 1) {
        burstData.push({
            id: id,
            hts: iid,
            xmd0: null,
            ymd0: null,
            xmd: null,
            ymd: null,
        });
    }

    if (iid > 1) {
        let xmd0 = 0,
            ymd0 = 0,
            xmd = 0,
            ymd = 0;
        let xmd0Arr = [],
            ymd0Arr = [],
            xmdArr = [],
            ymdArr = [];

        const currentBurst2 = shootingData.filter((element) => element.id == id);

        currentBurst2.forEach((element) => {
            xmd0Arr.push(Math.abs(element.x));
            ymd0Arr.push(Math.abs(element.y));
            xmdArr.push(element.x);
            ymdArr.push(element.y);
        });

        xmd0 = median(xmd0Arr);
        ymd0 = median(ymd0Arr);

        xmd = mdCalc(xmdArr);
        ymd = mdCalc(ymdArr);

        if (burstData[id - 1]) {
            burstData[id - 1] = {
                id: id,
                hts: iid,
                xmd0: xmd0,
                ymd0: ymd0,
                xmd: xmd,
                ymd: ymd,
            };
        }

        if (!burstData[id - 1]) {
            burstData.push({
                id: id,
                hts: iid,
                xmd0: xmd0,
                ymd0: ymd0,
                xmd: xmd,
                ymd: ymd,
            });
        }
    }
    return burstData;

};

export { burstDataCalc };
