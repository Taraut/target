import React from 'react';
import { Menu, Dropdown } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import { connect } from 'react-redux';
import { getLanguage, getTexts } from '../../redux/actions';
import { languageDefault } from '../../defaults';

class LanguageSelector extends React.Component {
    componentDidMount() {
        this.props.dispatch(getLanguage({ language: languageDefault }));
        this.texts(languageDefault);
    }

    onClick = ({ key }) => {
        this.props.dispatch(getLanguage({ language: key }));
        this.texts(key);
    };

    menu = () => {
        return (
            <Menu onClick={this.onClick}>
                <Menu.Item key="eng">English</Menu.Item>
                <Menu.Item key="rus">Русский</Menu.Item>
            </Menu>
        );
    };

    texts = (key) => {
        if (!languageDefault) {
            languageDefault = 'eng';
        }
        if (!key) {
            key = languageDefault;
        }

        import(`../../../translations/${key}`).then((texts) => {
            this.props.dispatch(getTexts({ texts: texts.translation }));
        });
    };

    render() {
        let { lang } = '';
        this.props.texts ? ({ lang } = this.props.texts) : ({ lang } = '');

        return (
            <Dropdown overlay={this.menu}>
                <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                    {lang} <DownOutlined />
                </a>
            </Dropdown>
        );
    }
}

function mapStateToProps(state) {
    return {
        language: state.itemsReducer.language,
        texts: state.itemsReducer.texts,
    };
}
export default connect(mapStateToProps)(LanguageSelector);
