exports.languageDefault = 'eng';

// media qeries in px
exports.mediaData = {
    // Mobile
    mobileMaxWidth: 767,
    // Tablet
    tabletMinWidth: 768,
    tabletMaxWidth: 1024,
};
