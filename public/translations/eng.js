exports.translation = {
    lang: 'English',
    home: {
        title: 'M16A2 25 Meter Zeroing Target, M16A2 Burst mode, 30-round magazine',
        tableId: 'Burst #',
        tableHts: 'Total burst hits',
        tableXmd0: 'Median deviation to 0, X axis',
        tableYmd0: 'Median deviation to 0, Y axis',
        tableXmd: 'Median deviation between hits, X axis',
        tableYmd: 'Median deviation between hits, Y axis',
        fire: 'FIRING LINE CLEAR',
        emStop: 'Emergency stop',
        emStopAlertMsg: 'EMERGECY STOP ACTIVATED',
        emStopAlertdSC: 'Application is locked. Restart for continue',
    },
};
